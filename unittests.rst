
lmdescrypt testing automation
-------------------------------------------------------

Script the following with VBoxManage commands:

* create a VM with ISO and new VDI disk, and 2nd hostonly NIC
* save a copy of the initial state of the VDI file
* start it
* install openssh-server
* set mint password
* savestate
* keep a copy of the .sav file (~/project/lmdescrypt/testy.sav)

Now we have our basic testing cycle:

* restore VDI file to the initial copy?  (not necessary)
* startvm from the saved state (adoptstate; startvm)
* ssh to the VM and run a desired lmdescrypt test (192.168.56.102)
* poweroff (no shutdown or reboot, as this detaches the ISO, causing restart from .sav to fail!)
* take a snapshot
* clonevm using this snapshot
* detach the DVD from the clone
* startvm the clone
* verify that it boots up via ssh
* shut it down
* delete the snapshot? (not necessary)

The only gotcha to watch out for is if you don't restore the VDI file, and the last install was an unencrypted LVM, the restarted system will auto-bind the LVM volume and all attempts to partition the disk will crash (because LVM is holding on to the existing partition).



