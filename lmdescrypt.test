#!/bin/bash
set +o xtrace +o verbose +o errexit -o errtrace
trap 'set +o xtrace +o verbose +o errexit +o errtrace; return 1' INT TERM
version=0.988

############################################################################
#
#  lmdescrypt 0.988
#
# Download shortlinks: https://4e4.win/lm or http://j.mp/lmdescrypt
#
# This script installs Linux Mint Debian Edition (201403), LMDE2 (201503 or
#  201701), LMDE3, or regular Linux Mint 17, 17.1/2/3, 18, 18.1/2/3, or 19,
#  either i686 or amd64, whether with MSDOS or GPT partitions, UEFI or not.
# The result: a fully LUKS encrypted system, with LVM2 volumes of root and
#  swap (and optionally: data) with optional boot partition
#  (with optional boot-from-iso-file).
#
# Shortlink to download the script: http://4e4.win/lmdescrypt
# Tutorial on Linux Mint community: https://community.linuxmint.com/tutorial/view/2265
# Gitlab page: https://gitlab.com/pepa65/lmdescrypt
# Questions? Email solusos@passchier.net or post an Issue on the gitlab page.
#
############################################################################
### CONTENTS:
## - INSTRUCTIONS for preparation and use
## - SETTINGS to be adjusted
## - FUNCTIONS used internally
## - MAIN action
############################################################################

############################################################################
#### INSTRUCTIONS
############################################################################
##
### 1. Boot the Live environment
##
### 2. Open a Terminal (Menu/Terminal of Ctrl-Alt-T) and enter:
##> sudo -i
##> wget 4e4.win/lmdescrypt
##
### 3. If needed, adapt the SETTINGS section:
##> nano lmdescrypt
##
### 4. Make sure all the partitions mentioned in SETTINGS exist
## For example, (re)partition the drive like this
##  (erasing all, taking up all space):
##
##> swapoff -a  # unmount all automounted swap partitions
##
##> sgdisk -Zon1::+2M -t1:ef02 -c1:BIOS -n 2::-0 -t2:8e00 -c2:X -g /dev/sda
##
## For a UEFI setup instead, this example works:
##
##> sgdisk -Zon1::+260M -t1:ef00 -c1:EFI -n2::-0 -t2:8e00 -c2:X -g /dev/sda
##
##  (This is giving almost the whole drive to the encrypted lvm2)
##
### 5. Start the script:
##
##> source lmdescrypt
##
### 6. Answer the questions as they come up:
##  - set password for encryption
##  Then after a wait for all the preparations to have happened:
##  - set password for user
##  - set timezone
##  - configure keyboard
##
### And that's it!
##
### Installing into a pre-existing environment
##  Using a pre-existing boot-partition, LUKS partition and LVM Logical Volumes
##   is entirely supported.
##  Not having a separate boot partition is also supported: total encryption!
##  Multiple booting with other OSes also works out of the box.
##  MBR, GPT partition tables and UEFI work according to configuration.
##
############################################################################

############################################################################
#### SETTINGS
############################################################################
### Review all settings in this section

### The device to be booted (for the grub bootloader: usually a drive!):
grub_device=/dev/sda

### turn on to repartition disk automatically to use whole disk
# if turned on, leave boot_part and crypt_part settings blank
auto_partition=1
# if auto-partitioning, set this to control UEFI vs. BIOS boot
# (if not auto-partitioning, ignored -- UEFI is auto-detected from existing partitioning)
use_uefi=0
### you can use this script with or without luks
use_luks=0
### use ZFS (bootable root) instead of LVM
use_zfs=0
# if $vg_label is not an existing zpool, it will be created with separate
# ZFS filesystems for root (/ = $vg_label/ROOT/$root_label), /home and /root.
# Otherwise the existing zpool will be used AS-IS, without ANY reformatting or
# deletion of files, so its root filesystem MUST NOT contain any system files
# that might clash with the new system install!!  You can ensure this via a command like
# zfs destroy -r $vg_label/ROOT/$root_label && zfs create $vg_label/ROOT/$root_label

#############################################################
### Under auto_partition=1, changing any settings below is OPTIONAL (not required),
### because auto_partition will adjust them automatically if necessary.

## Adjust if needed (not needed if following the suggested partitioning)
## Specify the partitions you created by any valid device path, like:
## /dev/disk/by-label/...  /dev/...  /dev/disk/by-id/...
## Total encryption: boot_part should be empty
## WITHOUT encryption (use_luks=0), leave boot_part and crypt_part empty.
## UEFI setup: Must have an efi partition (type ef00) of at least 260M with a
##  VFAT filesystem, will be formatted if not yet formatted!
## GPT (non-UEFI) setup: Must have a bios partition (type ef02) of at least 2M!
### Partitions to use:
boot_part=/dev/sda4
crypt_part=/dev/sda1
### without luks you must set the following partition for LVM/ZFS, otherwise leave blank
vg_part=

## When boot_part is not set, this is ignored
## If the boot partition is too small, the install will fail in the end!
### Set to 1 to include this iso as file on boot partition to boot from
include_isofile=0

## When booting with total encryption, it is necessary to enter the password
##  twice, once for grub and once for the kernel to decrypt the LUKS partition
## Entering a password only once to unlock is possible by using a keyfile that
##  is stored in the initrd file, but anyone can access it and then use it to
##  decrypt the LUKS-encrypted partition!
### Set to 1 to use an initrd keyfile --INSECURE!-- when doing total encryption:
insecure_keyfile=0

## Adjust only if you already have a LUKS encrypted partition that you want to wipe and start from scratch
## If set to 0 and there are pre-existing volumes that need to be wiped,
##  do it manually:
##   cryptsetup luksOpen <crypt_part> <crypt_label>
##   lvremove /dev/<vg_label>/<volumelabel>
### Wipe the crypt-partition before setup, even if already set up, if 1:
force_reencrypt=0

## No strict need to adjust; only takes effect if force_reencrypt (above) is 1
## WARNING: Putting 1 here will take a LONG time (depending on the size), let it run overnight!
### Check for bad blocks and fill with pseudorandom data if 1:
force_random=0

## No need to adjust unless re-using existing volumes!
## If a Volume Group or Logical Volume with this name EXISTS, IT WILL BE USED!
## The data_label will also be the name of the mountpoint
## If empty, the default NAME (in NAME_label) will be used
### Filesystem labels (no spaces):
boot_label=boot
crypt_label=luks1
vg_label=minty
root_label=ubuntu
swap_label=swap
data_label=

## Don't allot more space than available, and at least 6GB for root
## Specify lvm2 volume sizes in gigabytes (G) or megabytes (M), no decimal points or spaces, like: 10G or 200M
## - if empty (only for swap or data) it will NOT BE USED
## - if EXISTING then the Logical Volume will not be created and needs to exist already
##   (and so should the Volume Group then!)
## - if REST (only for root or data, not both) then the rest of the available space will be used
## - DEFAULT (only for swap) means 40% of RAM size (/sys/power/image_size)
##   (See: https://www.kernel.org/doc/Documentation/power/interface.txt)
### Logical volume usage & sizes:
root_size=REST
swap_size=DEFAULT
data_size=

## Adjust if re-using preexisting boot or data volumes!
## Fill in the desired filesystem-type, empty means it will NOT BE FORMATTED!
##  (pre-existing filesystem will be kept!)
## Supported for boot and root: btrfs ext2 ext3 ext4 reiserfs xfs; for swap: swap
## Supported for data: btrfs ext2 ext3 ext4 hfs hfsplus jfs minix msdos ntfs reiserfs vfat xfs
### Filesystems:
boot_fs=ext2
root_fs=ext4
swap_fs=swap
data_fs=

## Adjust if you require another encryption scheme, see /proc/crypto
## Find the speeds on your hardware by: cryptsetup benchmark (and choose accordingly)
## The -i parameter is the number of milliseconds spend iterating on THIS system
### Parameters for cryptsetup luksFormat:
cryptopts='-s 512 -h sha512 -i 5000'

## Adjust according to preference
### Username and hostname (no spaces):
username=mint
hostname=mine

## Adjust according to preference; internet connection required!
### Extra packages to be installed (in between the single quotes, seperated by space):
packages='openssh-server'

############################################################################
#### FUNCTIONS
############################################################################

Echo(){ ## $1=message
	echo -e '_______________________________________________________________________________\n'
	echo -e "  $1"
	echo -e '_______________________________________________________________________________\n'
}

## To really exit it needs to be followed by 'return' in the main body!
Exit(){ ## $1=message
	Echo "  ERROR: $1\n  ABORTING"
}

Warning(){ ## $1=message
	echo -e "\n  WARNING: $1\n"
}

Mkfs(){ ## $1=fs $2=label $3=dev $4=kind
	case $1 in
		hfs|hfsplus|jfs|minix|msdos|vfat|ntfs) [[ ! $4 = data ]] && return 2 ;;&
		## Continue even if the above pattern matched (unless $4 isn't data)
		btrfs) mkfs.$1 -f -L "$2" "$3" ;;
		ext2|ext3|ext4) mkfs.$1 -F -L "$2" "$3" ;;
		hfs) mkfs.$1 -h -v "$2" "$3" ;;
		hfsplus) mkfs.$1 -J -v "$2" "$3" ;;
		jfs) mkfs.$1 -q -L "$2" "$3" ;;
		minix) mkfs.$1 "$3" ;;
		msdos|vfat) mkfs.$1 -n "$2" "$3" ;;
		ntfs) mkfs.$1 -f -F -U -L "$2" "$3" ;;
		reiserfs) mkfs.$1 -ffl "$2" "$3" ;;
		xfs) mkfs.$1 -f -L "$2" "$3" ;;
		*) return 3 ;;
	esac
}

Checklabel(){ ## $1: labelname
	label="$1_label"
	content=$(eval "echo \$$label")
	[[ $content ]] || eval "$label=$1"
	[[ ! $content = ${content/ } ]] \
			&& Exit "Label $label contains spaces: '$content'"
}

############################################################################
#### MAIN
############################################################################

target='/target'

self=$(basename $BASH_SOURCE)

## If not run interactively, don't run
[[ $- != *i* ]] && Exit "  ERROR: This script needs to be sourced! Run it like:\n   sudo source $self\n  ABORTING" && exit 1
((EUID)) && Exit "This script only works when run with root privileges, like:\n   sudo source $self" && return 4

## automatic partioning, if requested
if ((auto_partition))
then
	echo "Repartitioning $grub_device automatically..."
	vgchange -an
	sleep 1
	sgdisk --zap-all $grub_device
	part1=1
	part3=3
	part4=4
	if ((use_uefi))
	then
		sgdisk -n3:1M:+512M -t3:EF00 $grub_device
		efi_part="$grub_device$part3"
	else
		sgdisk -a1 -n2:34:2047 -t2:EF02 $grub_device
	fi
	if ((use_luks))
	then
		if ((use_zfs))
		then
			sgdisk     -n4:0:+512M  -t4:8300 $grub_device
			sgdisk     -n1:0:0      -t1:8300 $grub_device
		else
			sgdisk     -n4:0:+512M  -t4:8300 $grub_device
			sgdisk     -n1:0:0      -t1:8e00 $grub_device
		fi
		force_reencrypt=1
		crypt_part="$grub_device$part1"
		boot_part="$grub_device$part4"
		vg_part=
	else
		if ((use_zfs))
		then
			sgdisk -n1:0:0 -t1:BF01 $grub_device
		else
			sgdisk -n1:0:0 -t1:8e00 $grub_device
		fi
		vg_part="$grub_device$part1"
		crypt_part=
		boot_part=
	fi
	partprobe
fi


## Check existence of mandatory devices
if ((use_luks))
then
	[[ ! -w "$crypt_part" ]] &&
		Exit "Crypt device does not exist or is not writable: '$crypt_part'" &&
		return 5
fi
[[ ! -w "$grub_device" ]] &&
	Exit "Grub device does not exist or is not writable: '$grub_device'" &&
	return 6

## Check names, labels, root_fs
[[ ! $username =~ ^[a-z_][a-z0-9_]*$ ]] &&
	Exit "Illegal format for username: '$username'" && return 7
[[ ! $hostname =~ ^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]$ ]] &&
	Exit "Illegal format for hostname: '$hostname'" && return 8
Checklabel crypt && return 9
Checklabel vg && return 10
Checklabel boot && return 11
Checklabel root && return 12
Checklabel swap && return 13
Checklabel data && return 14

if ((use_zfs))
then
	apt install --yes zfs-initramfs
	if [[ $swap_size = DEFAULT ]]
	then
		swap_size="2G"
	fi
else
	[[ ! $root_fs =~ ^(btrfs|ext2|ext3|ext4|reiserfs|xfs|)$ ]] &&
		Exit "Wrong value for root_fs: '$root_fs'" && return 15

## Determine root and data partition usage & size
	[[ $root_size ]] && [[ ! $root_size = REST ]] && [[ ! $root_size = EXISTING ]] &&
		[[ ! $root_size =~ [1-9][0-9]*[MG] ]] &&
		Exit "Invalid format root_size: '$root_size'" && return 16
	if [[ $data_size = REST ]]
	then
		[[ $root_size = REST ]] &&
			Exit "Settings data_size and root_size can't both be REST" && return 17
		lvm_data='-l 100%FREE -Zy'
	else
		if [[ $data_size && ! $data_size = EXISTING ]]
		then
			[[ ! $data_size =~ [1-9][0-9]*[MG] ]] &&
				Exit "Invalid format data_size: '$data_size'" && return 18
			lvm_data="-L $data_size -Zy"
		fi
	fi

## Set swap size
	if [[ $swap_size = DEFAULT ]]
	then
		lvm_swap="-L $(cat /sys/power/image_size)B -Cy -Zy"
	else
		if [[ ! $swap_size = EXISTING ]]
		then
			[[ $swap_size ]] && [[ ! $swap_size =~ [1-9][0-9]*[MG] ]] &&
				Exit "Invalid format swap_size: $swap_size" && return 19
			lvm_swap="-L $swap_size -Cy -Zy"
		fi
	fi
fi

## Check type of setup
gdisk=$(echo 2 |gdisk -l "$grub_device")
efi_part=
if ! grep -q ' GPT: not present$' <<<"$gdisk"
then  ## GPT partition scheme
	if [[ -e /sys/firmware/efi ]]
	then  ## UEFI: so it is an amd64 architecture that supports UEFI
		read n discard <<<"$(grep ' EF00  ' <<<"$gdisk" |head -1)"
		[[ -z $n ]] && Exit "UEFI without type ef00 efi partition!" && return 20
		efi_part="$grub_device$n"
		[[ ! $(lsblk "$efi_part" -no FSTYPE) = vfat ]] &&
			mkfs.vfat -n EFI "$efi_part"
		efi_partuuid=$(blkid -s PARTUUID -o value "$efi_part")
	else  ## non-UEFI GPT setup
		! grep -q ' EF02  ' <<<"$gdisk" &&
			Exit "Non-UEFI GPT setup without type ef02 bios partition" && return 21
	fi
fi

Echo "$self $version\n  Install Linux Mint with LUKS encryption"

## Encrypt
if ((use_luks))
then
	pwf=$(mktemp) pwf2=$(mktemp)
	chmod 600 "$pwf" "$pwf2"
	while [[ ! -s "$pwf" ]]
	do
		/lib/cryptsetup/askpass "Enter encryption password:" >"$pwf"
		echo
		if [[ -s "$pwf" ]]
		then
			/lib/cryptsetup/askpass "Enter the same password again:" >"$pwf2"
			echo
			diff -q --label $'\b\b\b\b\b\b\b\bFirst' "$pwf" --label "second password" "$pwf2" ||
				shred -u "$pwf"
			shred -u "$pwf2"
		else
			echo -e "Empty password not allowed\n"
		fi
	done

	if ((force_reencrypt))
	then
		((force_random)) && badblocks -c 10240 -s -w -t random -v "$crypt_part"
		cryptsetup luksFormat $cryptopts "$crypt_part" --key-file "$pwf"
	else
		cryptsetup isLuks "$crypt_part" &&
			Warning "$crypt_part is already formatted, reencryption not forced" ||
			cryptsetup luksFormat $cryptopts "$crypt_part" --key-file "$pwf"
	fi

	if [[ -z $boot_part ]] && ((insecure_keyfile))
	then
		key=crypto_keyfile.bin
		dd bs=512 count=4 if=/dev/urandom of="/root/$key"
		cryptsetup luksAddKey "$crypt_part" "/root/$key" --key-file "$pwf"
	fi

## Decrypt if not yet mapped
	crypt_dev="/dev/mapper/$crypt_label"
	[[ ! -b "$crypt_dev" ]] &&
		! cryptsetup luksOpen "$crypt_part" $crypt_label --key-file "$pwf" &&
		shred -u "$pwf" && Exit "Failed to open the encrypted partition $crypt_part" &&
		return 21
	shred -u "$pwf"
	crypt_uuid=$(blkid -s UUID -o value "$crypt_part")
fi

if ((use_zfs))
then
	# ensure not already mounted somewhere else
	if zfs list $vg_label
	then
			zpool export $vg_label
	fi
	if zpool import -R $target $vg_label
	then
		echo Using existing zpool $vg_label AS-IS with all its existing filesystems
		# ensure that $vg_label/ROOT/$root_label successfully mounts on $target/
		if zfs set canmount=on $vg_label/ROOT/$root_label
		then
			zpool export $vg_label
			zpool import -R $target $vg_label
		fi
	else
		if ((use_luks))
		then
			zpool create -f -o ashift=12 -O atime=off -O canmount=off -O compression=lz4 -O normalization=formD -O xattr=sa -O mountpoint=/ -R $target $vg_label $crypt_dev
		else
			zpool create -f -o ashift=12 -O atime=off -O canmount=off -O compression=lz4 -O normalization=formD -O xattr=sa -O mountpoint=/ -R $target $vg_label $vg_part
		fi

		zfs create -o canmount=off -o mountpoint=none $vg_label/ROOT
		zfs create -o canmount=noauto -o mountpoint=/ $vg_label/ROOT/$root_label
		zfs mount $vg_label/ROOT/$root_label
		zfs create -o setuid=off $vg_label/home
		zfs create -o mountpoint=/root $vg_label/ROOT/home
	fi
	zfs create -p -V $swap_size -b $(getconf PAGESIZE) -o compression=zle -o logbias=throughput -o sync=always -o primarycache=metadata -o secondarycache=none -o com.sun:auto-snapshot=false $vg_label/$swap_label
	swap_dev="/dev/zvol/$vg_label/$swap_label"
	mkswap -f $swap_dev
else
## Make lvm2 volumes if not there yet and make filesystems if wanted
	vgchange -ay
	if [[ ! "  $vg_label" = "$(vgs --noheadings -o vg_name)" ]]
	then
		if ((use_luks))
		then
			vgcreate -Zy $vg_label $crypt_dev
		else
			vgcreate -Zy $vg_label $vg_part
		fi
	fi
	root_dev="/dev/$vg_label/$root_label"
	data_dev="/dev/$vg_label/$data_label"
	swap_dev="/dev/$vg_label/$swap_label"

## Create & format swap if wanted
	if [[ $swap_size ]]
	then
		[[ ! -h $swap_dev ]] && lvcreate -Wy $lvm_swap -n $swap_label $vg_label
		if [[ $swap_fs ]]
		then
			[[ ! $swap_fs = swap ]] && Exit "Wrong value for swap_fs: '$swap_fs'" &&
				return 22 ||
				mkswap -f -L $swap_label "$swap_dev"
		fi
	fi

## Create and format root partition, and data partition if wanted
	[[ ! $root_size = REST ]] && [[ ! -h $root_dev ]] &&
		lvcreate -Wy -L $root_size -Zy -n $root_label $vg_label
	if [[ $data_size ]]
	then  ## Create if it doesn't exist and format if required
		[[ ! -h $data_dev && $data_size = EXISTING ]] &&
			Exit "There is no existing data device $data_dev" && return 23
			lvcreate -Wy $lvm_data -n $data_label $vg_label
		if [[ $data_fs ]]
		then
			! Mkfs "$data_fs" "$data_label" "$data_dev" data &&
				Exit "Unsupported filesystem $data_fs for data volume $data_dev with label $data_label" &&
				return 24
		else  ## No data filesystem specified: existing or bust
			if [[ $data_size = EXISTING ]]
			then
				! data_fs=$(lsblk "$data_dev" -no FSTYPE 2>/dev/null) &&
					Exit "No filesystem specified for data volumeand none present" &&
					return 25
			else
				Exit "No filesystem specified for data volume"
				return 26
			fi
		fi
	fi
	[[ $root_size = REST ]] && [[ ! -h $root_dev ]] &&
		lvcreate -Wy -l 100%FREE -Zy -n $root_label $vg_label
	if [[ $root_fs ]]
	then
		! Mkfs "$root_fs" "$root_label" "$root_dev" root &&
			Exit "Unsupported filesystem $root_fs for root-device $root_dev with label $root_label" &&
			return 27
	else
		root_fs=$(lsblk "$root_dev" -no FSTYPE 2>/dev/null)
		[[ -z $root_fs ]] &&
			Exit "Root volume is not has no filesystem, fill in 'root_fs'" &&
			return 28
		[[ $root_fs =~ ^(btrfs|ext2|ext3|ext4|reiserfs|xfs)$ ]] &&
			Exit "Root partition has unsupported filesystem: $root_fs" && return 29
	fi
fi


if [[ $boot_part ]]
then
	[[ ! $boot_fs =~ ^(btrfs|ext2|ext3|ext4|reiserfs|xfs|)$ ]] &&
		Exit "Wrong value for boot_fs: '$boot_fs'" && return 30
	if [[ $boot_fs ]]
	then
		! Mkfs "$boot_fs" "$boot_label" "$boot_part" boot &&
			Exit "Unsupported filesystem $boot_fs for boot-device $boot_part with label $boot_label" &&
			return 31
	else
		boot_fs=$(lsblk "$boot_part" -no FSTYPE 2>/dev/null)
		[[ -z $boot_fs ]] &&
			Exit "Boot partition $boot_part has no filesystem, fill in 'boot_fs' (or leave boot_part empty)" &&
			return 32
	fi
	boot_uuid=$(blkid -s UUID -o value "$boot_part")
fi

## Mount
if ((use_zfs))
then
	echo Installing into ZFS...
else
	[[ $swap_size ]] && swapon "$swap_dev" && swapon -s
	mkdir -p "$target"
	mount "$root_dev" "$target"
	if [[ $data_size ]]
	then
		mkdir -p "$target/$data_label"
		mount "$data_dev" "$target/$data_label"
	fi
fi
if [[ $boot_part ]]
then
	mkdir -p "$target/boot"
	mount "$boot_part" "$target/boot"
fi

## Copy
rofsde='/lib/live/mount/rootfs/filesystem.squashfs'
rofslm='/rofs'
if [[ -d /lib/live ]]
then  ## Debian Edition
	lmde=1
	rofs=$rofsde
else  ## regular Mint
	lmde=0
	rofs=$rofslm
fi
Echo "Copying the system files..."
cp -a "$rofs"/* "$target"
rm $target/etc/apt/sources.list .
! grep -q 'Linux Mint 19' /etc/os-release &&
	cp /etc/apt/sources.list $target/etc/apt/sources.list

## Prepare root
echo "proc /proc proc defaults 0 0" >"$target/etc/fstab"
if ((use_zfs))
then
	echo $swap_dev none swap defaults 0 0 >> $target/etc/fstab
	if [[ $efi_part ]]
	then
		echo PARTUUID=$efi_partuuid /boot/efi vfat nofail,x-systemd.device-timeout=1 0 1 >> $target/etc/fstab
	fi
	p3=none
	p4=luks,discard,initramfs
else
	[[ $root_fs = btrfs ]] &&
		echo "$root_dev / $root_fs defaults,compress 0 1" >>"$target/etc/fstab" ||
		echo "$root_dev / $root_fs defaults,relatime,errors=remount-ro 0 1" >>"$target/etc/fstab"
	[[ $swap_size ]] && echo "$swap_dev $swap_label swap sw 0 0" >>"$target/etc/fstab"
	[[ $data_size ]] && echo "$data_dev /$data_label $data_fs relatime 0 2" >>"$target/etc/fstab"
	p3=none p4=luks
fi

if [[ -z $boot_part ]]
then
	if ((insecure_keyfile))
	then
		keytarget="$target/etc/initramfs-tools/hooks/crypto_keyfile"
		mv "/root/$key" "$target/root/$key"
		chmod 000 "$target/root/$key"
		echo "cp /root/$key \"\${DESTDIR}\"" >"$keytarget"
		chmod +x "$keytarget"
		p3="/$key"
		p4="luks,keyscript=/bin/cat"
	fi
	sed -i '/^GRUB_TIMEOUT/a GRUB_ENABLE_CRYPTODISK=y' "$target/etc/default/grub"
	gcl="GRUB_CMDLINE_LINUX=\"cryptdevice=$crypt_part:$crypt_label\""
	sed -i "s@^GRUB_CMDLINE_LINUX=\"\"@$gcl@" "$target/etc/default/grub"
	sed -i 's@ splash"$@"@' "$target/etc/default/grub"
else
	echo "UUID=$boot_uuid /boot $boot_fs relatime 0 2" >>"$target/etc/fstab"
	if ((include_isofile))
	then
		cd=$(df |grep -o '^/dev/sr..')
		isoname=$(grep '^GRUB_TITLE=' "$target/etc/linuxmint/info")
		isoname=${isoname/GRUB_TITLE=/}
		isofile="/$(date +%s).iso"
		Echo "Copying the iso..."
		dd if=$cd of="$target/boot$isofile"
		(cd "$target/boot"; ln -s "$isofile" "$isoname")
		((lmde)) && dir=live || dir=casper
		sdxn=$(readlink -e "$boot_part")
		x=$(grep -o '/sd.' <<<"$sdxn")
		root="hd$(($(printf '%d' "'${x: -1}")-97)),msdos${sdxn//[^0-9]/}"
		echo "ISO $isoname is on ($root)$isofile"
		cat <<-X >>"$target/etc/grub.d/40_custom"
			menuentry "Boot from ISO $isoname ($isofile)" {
			  set root='$root'
			  set isofile='$isofile'
			  loopback loop \$isofile
			  linux (loop)/$dir/vmlinuz boot=$dir iso-scan/filename=\$isofile noeject noprompt splash -- 
			  initrd (loop)/$dir/initrd.lz
			}
		X
	fi
fi
sed -i 's/^GRUB_TIMEOUT=10/GRUB_TIMEOUT=4/' "$target/etc/default/grub"
sed -i 's/^GRUB_HIDDEN_TIMEOUT=0/#GRUB_HIDDEN_TIMEOUT=0/' "$target/etc/default/grub"
if ((use_luks))
then
	echo "$crypt_label UUID=$crypt_uuid $p3 $p4" >"$target/etc/crypttab"
fi
sed "s/mint/$hostname/g" /etc/hosts >"$target/etc/hosts"
echo $hostname >"$target/etc/hostname"
echo "$LANG UTF-8" >>"$target/etc/locale.gen"
mdm=/etc/mdm/mdm.conf
ldm=/etc/lightdm/lightdm.conf
[[ -f $mdm ]] && dm=$mdm || dm=$ldm
sed "s/mint/$username/g" "$dm" >"$target$dm"
rm -- "$target/etc/resolv.conf"  ## remove symlink
cp /etc/resolv.conf "$target/etc"
if ((use_luks))
then
	cryptsetup luksHeaderBackup --header-backup-file "$target/root/$crypt_label.luksHeaderBackup" "$crypt_part"
	chmod 400 "$target/root/$crypt_label.luksHeaderBackup"
fi
cp "$(readlink -e $BASH_SOURCE)" "$target/root/$self"
((lmde)) &&
	cp /lib/live/mount/medium/live/vmlinuz "$target/boot/vmlinuz-$(uname -r)" ||
	cp /cdrom/casper/vmlinuz "$target/boot/vmlinuz-$(uname -r)"
mount --bind /dev "$target/dev"

## Prepare the chroot script
inside="/root/$self.inside"
(cat <<X
#!/bin/bash
set +o verbose -o errtrace
export PS1='\[\033[01;33m\]\w \[\033[01;32m\]\$ \[\033[00m\]'
hash -l lvmetad && lvmetad -p /lvm.pid
mount -t sysfs sys /sys
mount -t proc proc /proc
mount -t devpts pts /dev/pts
pvscan --cache
locale-gen --purge --no-archive
type -p localectl >/dev/null && localectl set-locale LANG=$LANG
useradd -ms /bin/bash $username
pwf=\$(mktemp) pwf2=\$(mktemp)
chmod 600 "\$pwf" "\$pwf2"
while [[ ! -s "\$pwf" ]]
do
	/lib/cryptsetup/askpass "Enter password for user $username: " >"\$pwf"
	echo
	if [[ -s "\$pwf" ]]
	then
		/lib/cryptsetup/askpass "Enter the same password again: " >"\$pwf2"
		echo
		diff -q --label \$'\b\b\b\b\b\b\b\bFirst' "\$pwf" --label "second password"	"\$pwf2" ||
			shred -u "\$pwf"
		shred "\$pwf2"
	else
		echo -e "Empty password not allowed\n"
	fi
done
chpasswd <<<"$username:\$(cat "\$pwf")"
addgroup $username sudo
X
((lmde)) && echo 'chpasswd <<<"root:$(cat "$pwf")"' || echo 'passwd -l root'
cat <<X
shred -u "\$pwf"
dpkg-reconfigure tzdata
dpkg-reconfigure keyboard-configuration 2>/dev/null
if [[ "$efi_part" ]]
then
	mkdir /media/cdrom
	mount /dev/sr0 /media/cdrom
	apt-cdrom -m add -d=/media/cdrom
	sed -i 's/^deb /deb [arch=amd64] /g' /etc/apt/sources.list
fi
if [[ "$packages" ]]
then
	[[ "$efi_part" ]] || apt-get update
	apt-get --yes install $packages
fi
X
if ((use_zfs))
then
	echo "apt install --yes --no-install-recommends linux-image-generic"
	echo "apt install --yes zfs-initramfs"
	echo "grub-probe /"
	echo "update-initramfs -c -k all"
fi
if ((lmde))
then
	echo "apt-get -qq purge '^live-.*'"
	echo 'rm -rvf /lib/live'
	echo "update-initramfs -c -k $(uname -r)"
fi
cat <<X
chmod 600 "/boot/initrd.img-$(uname -r)"
if [[ "$efi_part" ]]
then
	mkdir -p /boot/grub/efi/EFI/boot
	mount "$efi_part" /boot/grub/efi
	DEBIAN_FRONTEND=noninteractive apt install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --yes --force-yes grub-efi mokutil
	grub-install --force --recheck --efi-directory=/boot/grub/efi "$grub_device"
	grep -q '^Linux Mint 17' /etc/issue && sed -i 's/^quick_boot="1"/quick_boot="0"/g' /etc/grub.d/*
	update-grub
	cp /boot/grub/efi/EFI/*/grubx64.efi /boot/grub/efi/EFI/boot/bootx64.efi
	umount /boot/grub/efi
	umount /media/cdrom
	rmdir /media/cdrom
else
	update-grub
	grub-install --force --recheck "$grub_device"
fi
sed -i 's/^deb.* cdrom:.*/#\0/' /etc/apt/sources.list
gconftool-2 --set /apps/gksu/sudo-mode --type bool true
umount /dev/pts
umount /proc
umount /sys
[[ -f /lvm.pid ]] && kill -9 \$(cat /lvm.pid) && rm /lvm.pid
exit 0
X
) >"$target$inside"
chmod +x "$target$inside"

## Enter the fresh installation to finish up
Echo "Finalizing settings on the install"
chroot "$target" "$inside"

[[ $(df --output=pcent $target |tail -1) = 100% ]] &&
	Exit "The filesystem on $root_dev is 100% full" && return 33
[[ $boot_part && $(df --output=pcent $target/boot |tail -1) = 100% ]] &&
	Exit "The filesystem on $boot_part is 100% full" && return 34

if ((use_zfs))
then
	if ! zfs set canmount=noauto $vg_label/ROOT/$root_label
	then
		echo Please run zfs set canmount=noauto YOUR/ZFS/ROOT-FILESYSTEM after first boot
	fi
	echo "unmounting ZFS..."
	mount | grep -v zfs | tac | awk '/\/target/ {print $3}' | xargs -i{} umount -lf {}
	zpool export $vg_label
else
	umount "$target/dev"
	[[ $boot_part ]] && umount "$target/boot"
	[[ $data_size ]] && umount "$target/$data_label"
	umount "$target"
	[[ $swap_size ]] && swapoff "$swap_dev"
	sync
	sleep 1
	! vgchange -an && Exit "Not able to close all logical volumes" && return 35
	sync
	sleep 1
fi
if ((use_luks))
then
	! cryptsetup luksClose $crypt_label && Exit "Not able to cleanly close crypt" &&
		return 36
fi
sync

Echo "Ready for reboot!"

return 0
